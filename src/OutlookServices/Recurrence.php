<?php

namespace Office365\PHP\Client\OutlookServices;

use Office365\PHP\Client\Runtime\ClientValueObject;

/**
 * Class Recurrence
 *
 * @package Office365\PHP\Client\OutlookServices
 */
class Recurrence extends ClientValueObject
{

    public function __construct(RecurrencePattern $pattern, RecurrenceRange $range)
    {
        $this->Pattern = $pattern;
        $this->Range = $range;
        parent::__construct();
    }

    /**
     * @var RecurrencePattern
     */
    public $Pattern;

    /**
     * @var RecurrenceRange
     */
    public $Range;
}